
from django.core.management.base import BaseCommand
import random
from askapp.models import Profile, Question, Answer, Tag, Vote, Answer
from django.contrib.auth.models import User
from optparse import make_option
from faker.lorem import sentence, sentences
from mixer.fakers import get_username, get_email
from pprint import pformat
from django.db.models import Min, Max
from faker import Factory

class Command(BaseCommand):
	option_list = BaseCommand.option_list + (
		make_option('--users',
			action='store',
			dest='users',
			default=0
			),
		make_option('--questions',
			action='store',
			dest='questions',
			default=0
			),
		make_option('--tags',
			action='store',
			dest='tags',
			default=0
			),
		make_option('--answers',
			action='store',
			dest='answers',
			default=0
			),			
        )

   

	def handle(self, *args, **options):
		names = {}
		while(len(names.keys()) < int(options['users'])):
			names[get_username(length = 30)] = 1
		for name in names.keys():
			try:
				u = User.objects.create(username = name, email = get_email())
				p = Profile.objects.create(user_id = u.id, rating = random.randint(0, 20))
			except:
				pass
        
		fake = Factory.create()
		for i in range(0, int(options['tags'])):
			try:
				tg = Tag.objects.create(tag = fake.word())
			except:
				pass	
				

		p_min = Profile.objects.all().aggregate(Min('id'))['id__min']
		p_max = Profile.objects.all().aggregate(Max('id'))['id__max']
		tg_min = Tag.objects.all().aggregate(Min('id'))['id__min']
		tg_max = Tag.objects.all().aggregate(Max('id'))['id__max']
		
		for i in range(0, int(options['questions'])):
			n_likes = random.randint(0, 20)
			n_dislikes = random.randint(0, 5)
			q = Question.objects.create(author_id = random.randint(p_min, p_max), 
				title = (sentence())[0:59], text = sentences(3)
				,date_added=fake.date_time_this_decade())
			for j in range(0, random.randint(0, 3)):
				try:
					tg = Tag.objects.get(pk=random.randint(tg_min, tg_max))				
					q.tags.add(tg)
				except:
					pass	
			for j in range(0, n_likes):
				try:
					v = Vote.objects.create(author_id = random.randint(p_min, p_max),to_question_id=q.id,is_like=True)
				except:
					pass	
								
			for j in range(0, n_dislikes):
				try:
					v = Vote.objects.create(author_id = random.randint(p_min, p_max),to_question_id=q.id,is_like=False)
				except:
					pass	
			likes = (Vote.objects.filter(to_question_id=q.id, is_like=True).count())
			dislikes = (Vote.objects.filter(to_question_id=q.id, is_like=False).count())
			q.rating = likes-dislikes
			q.save()
			
		q_min = Question.objects.all().aggregate(Min('id'))['id__min']
		q_max = Question.objects.all().aggregate(Max('id'))['id__max']
			
		for i in range(0, int(options['answers'])):
			try:
				a = Answer.objects.create(author_id=random.randint(p_min, p_max),to_question_id=random.randint(q_min, q_max),
					text=sentences(3),date_added=fake.date_time_this_decade())
			except:
				pass		
			
			
			
	


