from askapp.models import Profile, Tag
from django import template
register = template.Library()


@register.inclusion_tag("inc/best_users.html")
def best_users():	
	return {'best_users' : Profile.objects.all().order_by('-rating')[0:5]}

@register.inclusion_tag("inc/best_tags.html")
def best_tags():
	
	tags = Tag.objects.raw('select tag_id as id, count(tag_id) as num '
							+'from askapp_question_tags group by tag_id '
						+'order by num desc limit 10')
						
	return {'best_tags' : tags}
