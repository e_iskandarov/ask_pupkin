from django.shortcuts import render, render_to_response, redirect
from askapp.models import Question, Answer, Profile, Tag, Vote
from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist  
from django.template import RequestContext
from askapp.forms import QuestionForm, RegForm, AnswerForm, SettingsForm
from django.contrib.auth.models import User
from django.utils.http import is_safe_url
from django.shortcuts import resolve_url
from django.conf import settings
from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth import login as auth_login
from django.http import JsonResponse

def like(request):
	if request.method != 'POST':
		raise Http404 
	resp = {}
	
	is_like = (request.POST['like'] == "1")
	q = Question.objects.get(pk = request.POST['q_id'])
	
	try:
		v = Vote.objects.get(author_id = request.POST['auth_id'], to_question_id=request.POST['q_id'])
		if v.is_like == is_like:
			resp['status'] = 'error'
			if is_like:
				resp['error'] = 'you already liked this question'
			else:
				resp['error'] = 'you already disliked this question'			
			return JsonResponse(resp)	
		else:
			v.delete()								
	except:
		Vote.objects.create(author_id = request.POST['auth_id'], to_question_id=request.POST['q_id'],is_like=is_like)
		
	if is_like:
		q.rating += 1
	else:
		q.rating -= 1
	q.save()
	
	resp['new_rating'] = q.rating
	resp['status'] = 'ok'
	return JsonResponse(resp)

def best(request):
	if request.method != 'POST':
		raise Http404 
	resp = {}	
	
	q = Question.objects.get(pk = request.POST['q_id'])
	if q.best_answer != None:
		resp['status'] = 'error'
		resp['error'] = 'best answer for this question is already chosen'
		return JsonResponse(resp)
		
	a = Answer.objects.get(pk = request.POST['a_id'])
	q.best_answer = a
	q.save()
	
	resp['status'] = 'ok'
	return JsonResponse(resp)

def pagination_init(page, content, context, items_per_page):	   
	paginator = Paginator(content, items_per_page)
	try:
		result = paginator.page(page)
		intpage = int(page)
	except PageNotAnInteger:
		result = paginator.page(1)
		intpage = 1
	except EmptyPage:
		result = paginator.page(paginator.num_pages)
		intpage = paginator.num_pages    
	context['prev'] = []
	for i in range(max(1, intpage - 2), intpage):
		context['prev'].append(i)   
	if(intpage < paginator.num_pages - 1):
		context['dotdotdot'] = True
	else:
		context['dotdotdot'] = False
	return result

def index(request, sort = ''):
	
	context = {}
	questions = {}
	tag = request.GET.get('tag')
	page = request.GET.get('page')
	
	try:
		tg = Tag.objects.get(tag = tag)
		all_questions = Question.objects.filter(tags = tg)
	except:
		tg = None
		all_questions = Question.objects.all()	
			
		
	if(sort == 'recent' or sort == ''):
		all_questions = all_questions.order_by('-date_added')
		context['header'] = 'Recent questions'
	elif(sort == 'hot'):
		all_questions = all_questions.order_by('-rating')
		context['header'] = 'Hot questions'
	else:
		raise Http404
	
	if tg != None:		
		context['header'] += ' with tag "' + tag + '"'
	context['header'] += ':'
	
	context['questions'] = pagination_init(page, all_questions, context, 30) 
	
	context['question_brief'] = True
	return render_to_response('main.html', context, context_instance=RequestContext(request))

def tag(request, tag = ''):
    
    context = {}
    questions = {}
    
    page = request.GET.get('page')
    
    
    #all_questions = Question.objects.filter
    tg = Tag.objects.get(tag = tag)
    all_questions = Question.objects.filter(tags = tg)
   
    
    context['questions'] = pagination_init(page, all_questions, context, 30) 
    
    context['question_brief'] = True
    return render_to_response('main.html', context, context_instance=RequestContext(request))

def question(request, qid = 0):
	context = {}
	answers = {}
    
	question = Question.objects.get(pk=qid)
      
	context['q'] = question;
    
	if request.user.is_authenticated():
		if request.method == 'POST':		
			
			ans = Answer(author = Profile.objects.get(user = request.user), to_question = question)
			form = AnswerForm(request.POST, instance=ans)
			if form.is_valid():
				form.save()
				request.user.profile.rating += 1
				request.user.profile.save()
		else:
			form = AnswerForm()
		context['form'] = form;
	
    
    # get answers, make best first
	answers = list(question.answer_set.exclude(pk=question.best_answer_id).order_by('-date_added'))		
	if question.best_answer != None:		
		answers = [question.best_answer] + answers
	context['best_exists'] = question.best_answer != None;
	
    
	page = request.GET.get('page') 
	context['answers'] = pagination_init(page, answers, context, 30) 
    
    	 
     
	context['question_brief'] = False
	return render_to_response('question.html', context, context_instance=RequestContext(request))
    

def profile(request):
	if request.user.is_authenticated():	
		if request.method == 'POST':		
			form = SettingsForm(request.user.id, request.POST, request.FILES)			
			if form.is_valid():
				request.user.username = form.cleaned_data['username']			
				request.user.email = form.cleaned_data['email']				
				if len(form.cleaned_data['password']) > 0:
					request.user.set_password(form.cleaned_data['password'])
					update_session_auth_hash(request, request.user)
				request.user.save()
				
				profile = Profile.objects.get(user = request.user)
				if request.POST.get('remove', False):
					profile.avatar = None					
				else:	
					profile.avatar = request.FILES.get('avatar', profile.avatar)				
				profile.save()
				
				
		else:		
			form = SettingsForm(request.user.id, initial={
				'username': request.user.username,
				'email': request.user.email,
				'avatar': request.user.profile.avatar,
				})
		return render_to_response('settings.html', {'form' : form}, context_instance=RequestContext(request))
	else:
		return redirect('/login/?next=/profile/')

def login(request): 
    context = {}
    return  render(request, 'login.html',context)
   
def ask(request):
	context = {}
	if not request.user.is_authenticated():
		return redirect('/login/?next=/ask/')
	if request.method == 'POST':
		q = Question(author = Profile.objects.get(user = request.user), rating = 0)
		form = QuestionForm(request.POST, instance=q)
		
		if form.is_valid():
			question = form.save()
			tags = form.cleaned_data['tags']		
			for i in range(0, len(tags)):
				try:
					tg = Tag.objects.get(tag = tags[i])
					question.tags.add(tg)
				except ObjectDoesNotExist :
					tg = Tag.objects.create(tag=tags[i])
					question.tags.add(tg)
			question.save()
			request.user.profile.rating -= 1
			request.user.profile.save()
			return redirect('/question/'+str(question.id)+'/')
	else:
		form = QuestionForm()
	
	return render(request, 'ask.html', {'form': form})

def signup(request):
	context = {}
	if request.user.is_authenticated():		
		return redirect('askapp.views.index')
	redirect_to = request.POST.get('next', request.GET.get('next', '/profile/'))	
	if request.method == 'POST':
		
		form = RegForm(request.POST, request.FILES)
		
		if form.is_valid():
			user = User.objects.create_user(form.cleaned_data['username']
				,form.cleaned_data['email'], form.cleaned_data['password'])
			profile = Profile.objects.create(user = user, rating = 0)
			profile.avatar = request.FILES.get('avatar', None)
			profile.save()
			user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
			auth_login(request, user)
			if not is_safe_url(url=redirect_to, host=request.get_host()):
				redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
                
			return redirect(redirect_to)
	else:		
		form = RegForm()
	
	return render(request, 'signup.html', {'form': form})
