# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('askapp', '0008_auto_20141207_1131'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_like', models.BooleanField(default=False)),
                ('author', models.ForeignKey(to='askapp.Profile')),
                ('to_question', models.ForeignKey(to='askapp.Question')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='question',
            name='votes',
            field=models.ManyToManyField(related_name=b'question_votes', through='askapp.Vote', to='askapp.Profile'),
            preserve_default=True,
        ),
    ]
