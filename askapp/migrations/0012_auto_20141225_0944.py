# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('askapp', '0011_question_rating'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='avatar_url',
        ),
        migrations.AddField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(default=b'avatars/none.jpg', upload_to=b'avatars/'),
            preserve_default=True,
        ),
    ]
