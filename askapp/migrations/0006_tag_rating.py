# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('askapp', '0005_auto_20141201_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='rating',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
