# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('askapp', '0006_tag_rating'),
    ]

    operations = [
        migrations.RenameField(
            model_name='answer',
            old_name='question',
            new_name='to_question',
        ),
        migrations.RemoveField(
            model_name='answer',
            name='is_right',
        ),
        migrations.AddField(
            model_name='question',
            name='best_answer',
            field=models.OneToOneField(null=True, to='askapp.Answer'),
            preserve_default=True,
        ),
    ]
