# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('askapp', '0007_auto_20141206_2011'),
    ]

    operations = [        
        migrations.RemoveField(
            model_name='answer',
            name='rating',
        ),
        migrations.RemoveField(
            model_name='question',
            name='rating',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='rating',
        ),
    ]
