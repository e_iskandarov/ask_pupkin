from django.db import models
#from django.contrib.auth.models import User
from django.conf import settings

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    rating = models.IntegerField(default = 0)
    avatar = models.ImageField(upload_to = 'avatars/', null = True)
    def safe_avatar(self):
        if self.avatar:
            return self.avatar.url
        else:
            return '/noAvatar.jpeg'

class Tag(models.Model):
    tag = models.CharField(max_length = 33, unique = True)

class Question(models.Model):
    author = models.ForeignKey(Profile)
    title = models.CharField(max_length = 60)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add = True)
    tags = models.ManyToManyField(Tag)
    best_answer = models.OneToOneField('Answer', null=True)
    votes = models.ManyToManyField(Profile, through='Vote', related_name='question_votes')
    rating = models.IntegerField(default = 0)
    def get_rating(self):
        likes = (Profile.objects.filter(vote__to_question=self, vote__is_like=True).count())
	dislikes = (Profile.objects.filter(vote__to_question=self, vote__is_like=False).count())
        return likes - dislikes

class Vote(models.Model):
    author = models.ForeignKey(Profile)
    to_question = models.ForeignKey(Question)
    is_like = models.BooleanField(default = False)
    class Meta:
        unique_together = ["author", "to_question"]

class Answer(models.Model):
    author = models.ForeignKey(Profile)
    to_question = models.ForeignKey(Question)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add = True)
    #is_right = models.BooleanField(default = False)


