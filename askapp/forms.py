from django.forms import ModelForm, ModelChoiceField
from askapp.models import Question, Tag, Answer, Profile
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.core.exceptions import ObjectDoesNotExist 
from django.contrib.auth.models import check_password

class AnswerForm(ModelForm):	
	class Meta:
		model = Answer
		fields = ['text']
		widgets = {
            'text': forms.Textarea(attrs={'class': 'textarea width100', 'rows' : '3', 'placeholder':'Enter your answer here...'}),
        }	

class RegForm(forms.Form):
	username = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Enter your username here...',
		'class' : 'input-xlarge'}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Enter your password here...',
		'class' : 'input-xlarge'}))
	repeat_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Repeat password...',
		'class' : 'input-xlarge'}))
	email = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Enter your e-mail here...',
		'class' : 'input-xlarge'}))
	avatar = forms.ImageField(required = False)
	
	
	def clean_repeat_password(self):
		if self.cleaned_data['password'] != self.cleaned_data['repeat_password'] :
			raise forms.ValidationError("The passwords you entered didn't match")
			
		return self.cleaned_data['repeat_password']	
	
	def clean_username(self):
		try:
			u = User.objects.get(username = self.cleaned_data['username'])		
			raise forms.ValidationError("This username is already taken")
		except ObjectDoesNotExist:
			return self.cleaned_data['username']
		
		
class SettingsForm(RegForm):
	user_id = 0
	def __init__(self, user_id, *args, **kwargs):
		super(SettingsForm, self).__init__(*args, **kwargs)		
		self.user_id = user_id
		
		self.fields['password'].required = False
		self.fields['repeat_password'].required = False
		self.fields['password'].widget.attrs['placeholder'] = 'Enter a new password here or leave empty'
		self.fields['password'].label = 'New password'
		

	current_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Enter your current password...',
		'class' : 'input-xlarge'}))
	avatar = forms.ImageField(required = False, widget=forms.FileInput)
	def clean_current_password(self):
		user = User.objects.get(pk = self.user_id)
		if not user.check_password(self.cleaned_data['current_password']):
			raise forms.ValidationError("Invalid password")			
		return self.cleaned_data['current_password']
		
	def clean_username(self):
		try:
			u = User.objects.get(username = self.cleaned_data['username'])	
			if u.pk != self.user_id:
				raise forms.ValidationError("This username is already taken")
		except ObjectDoesNotExist:
			pass
		return self.cleaned_data['username']
		

class MyAuthenticationForm(AuthenticationForm):    
	def __init__(self, *args, **kwargs):
		super(MyAuthenticationForm, self).__init__(*args, **kwargs)
		self.fields['password'].widget.attrs['placeholder'] = 'Enter your password here...'
		self.fields['username'].widget.attrs['placeholder'] = 'Enter your username here...'
    #username = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Enter your username here...'}))
    #password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Enter your password here...'}))
    
class QuestionForm(ModelForm):
	tags = forms.CharField(required=False, 
		widget=forms.TextInput(attrs={'class': 'ask_tags span9', 'placeholder':'Example: tag1 tag2 tag3'}))
	class Meta:
		model = Question
		fields = ['title', 'text']
		widgets = {
            'text': forms.Textarea(attrs={'class': 'ask_text span9', 'rows' : '6', 'placeholder':'Enter your question here...'}),
            'title': forms.TextInput(attrs={'class': 'ask_title span9', 'placeholder':'Enter title here...'}),
        }
	def clean_tags(self):
		error_messages = []
		
		cleaned_tags = self.cleaned_data['tags'].split()
		
		if len(cleaned_tags) > 3:
			error_messages.append("you cannot add more than 3 tags")
		tag_len_err = False
		for i in range(0, len(cleaned_tags)):
			if len(cleaned_tags[i])>33:
				tag_len_err = True
		if tag_len_err:
			error_messages.append("tags must be less than 34 characters long")
		if len(error_messages):
			raise forms.ValidationError(error_messages)
		else:
			return cleaned_tags

    	
				
	
	
