from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
@csrf_exempt
def hello_world_post_test(request):
    
    if(request.method=='GET'):
        output = '<form method="post"><input type="text" name="test"><input type="submit"></form>'
        return HttpResponse(output)
    if(request.method=='POST'):      
        output='hello world <br/>POST data:<br/> '
	for key,value in request.POST.items():
		output += key + ' = ' + value + '<br/>'
        return HttpResponse(output)
