from django.conf.urls import patterns, include, url

from askapp.views import index, question, login, signup, profile, ask, like, best, tag
from askapp.forms import MyAuthenticationForm


urlpatterns = patterns('',
	url(r'^ask/$', ask), 
	url(r'^like/$', like),
	url(r'^best/$', best),
    url(r'^login/$', 'django.contrib.auth.views.login', {'authentication_form':MyAuthenticationForm}),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login'),
    url(r'^signup/$', signup),
    url(r'^settings/$', profile),
    url(r'^profile/$', profile),
    url(r'^question/(?P<qid>\d+)/$', question),
    url(r'^(?P<sort>\w+)/$', index),                
    url(r'/$', index),  
)


