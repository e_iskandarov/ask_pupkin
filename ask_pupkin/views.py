from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import render


def hello_world(request):    
    if(request.method=='GET'):
	output='hello world <br/>GET data:<br/> '
	for key,value in request.GET.items():
		output += key + ' = ' + value + '<br/>'         
        return HttpResponse(output)
    if(request.method=='POST'):      
        output='hello world <br/>POST data:<br/> '
	for key,value in request.POST.items():
		output += key + ' = ' + value + '<br/>'
        return HttpResponse(output)


